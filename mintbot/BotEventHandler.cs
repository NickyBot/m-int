﻿using Discord.Commands;
using Discord.WebSocket;
using System;
using System.Reflection;
using System.Threading.Tasks;

namespace mintbot
{
	class BotEventHandler
	{
		DiscordSocketClient _client;
		CommandService _cmdservice;

		public async Task InitializeAsync(DiscordSocketClient client)
		{
			_client = client;
			_cmdservice = new CommandService();

			await _cmdservice.AddModulesAsync(Assembly.GetEntryAssembly(), null);
			_client.MessageReceived += HandleCommandAsync;
		}

		private async Task HandleCommandAsync(SocketMessage s)
		{
			var msg = s as SocketUserMessage;
			if (msg == null) return;
			var context = new SocketCommandContext(_client, msg);
			if (context.User.IsBot) return;


			int argPos = 0;
			if (msg.HasStringPrefix(Config.bot.cmdPrefix, ref argPos)
				|| msg.HasMentionPrefix(_client.CurrentUser, ref argPos))
			{
				var result = await _cmdservice.ExecuteAsync(context, argPos, null);
				if (!result.IsSuccess && result.Error != CommandError.UnknownCommand)
				{
					Console.WriteLine(result.ErrorReason);
				}
			}
		}
	}
}
