﻿using Discord.Commands;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace mintbot
{
	public class MiscCommands : ModuleBase<SocketCommandContext>
	{
		[Command("ping", RunMode = RunMode.Async)]
		[Alias("pingme", "pong")]
		public Task PingAsync()
			=> ReplyAsync("pong!");

		[Command("stop", RunMode = RunMode.Async)]
		[Alias("shutdown","exit")]
		[RequireOwner]
		public async Task StopAsync()
		{
			await ReplyAsync("Bye bye");
			await Context.Client.LogoutAsync();
			Thread.Sleep(100);
			Environment.Exit(0);	
		}


	}
}
